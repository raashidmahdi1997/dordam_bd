Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'DELL-5000','LAPTOP','DELL',to_date('20100725','YYYYMMDD'),'2 YEARS',43500.99,'RAM-4GB,CORE i5',
'http://i.dell.com/das/dih.ashx/527x340/sites/imagecontent/products/publishingimages/polaris/21962-home-laptop-inspiron-15-5559-banner-3-600x417.jpg',
'https://www.youtube.com/watch?v=qsCmwZE58gw');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'ASUS-X55LF','LAPTOP','ASUS',to_date('20100725','YYYYMMDD'),'2 YEARS',59501.99,'RAM-8GB,CORE i5',
'https://i.ytimg.com/vi/VpvBn-iI_CM/maxresdefault.jpg',
'https://www.youtube.com/watch?v=YsnlUsPv5rM');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB',
'https://mobilemablive.com/Walton/Walton%20%20%20Primo%20RX4.jpg',
'https://www.youtube.com/watch?v=mbGKUO5DOIU');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'GALAXY TAB PRO','TAB','SAMSUNG',to_date('20150715','YYYYMMDD'),'2 YEARS',75000,'RAM-4GB,memory-128GB',
'https://cnet4.cbsistatic.com/img/rEASQIez7GX1mqRc2nhqC5ZxVGQ=/fit-in/970x0/2016/03/23/f40f58ec-ed1f-476d-b420-5b916762b8ff/samsung-galaxy-tabpro-s-14.jpg',
'https://www.youtube.com/watch?v=xWrVLDXBgGk');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'IPHONE X','PHONE','APPLE',to_date('20100725','YYYYMMDD'),'2 YEARS',116000.99,'RAM-3GB,memory-64GB',
'https://zdnet1.cbsistatic.com/hub/i/r/2017/11/10/cdb3f636-7077-4028-80f3-bacd77f680da/resize/770xauto/6595120aec869aae223dbde80cd8fe44/iphone-x-notch.jpg',
'https://www.youtube.com/watch?v=OvVKnC6gGtg');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'GREE AC','AC','GREE',to_date('20160725','YYYYMMDD'),'2 YEARS',12000.99,'2 TONS',
'https://acmartbd.com/wp-content/uploads/2015/10/Gree-1.5-ton-Inverter-ac-924x784.jpg',
'https://www.youtube.com/watch?v=DzVJiSQNbew');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'LG FRIDGE','ELECTRICAL','LG',to_date('20140705','YYYYMMDD'),'10 YEARS',140000.99,'BIG FRDGE',
'http://bpc.h-cdn.co/assets/16/34/1471964793-lg-fridge.jpeg',
'https://www.youtube.com/watch?v=ZZViyi_MgI8');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'MERCEDES BENZ S CLASS','CAR','MERCEDES',to_date('20170715','YYYYMMDD'),'NO',13000000,'CLASSIC CAR WITH 700HP',
'http://www.carbuzz.com/resizeimg/imageshandler.ashx?w=640&h=480&url=http://db.carbuzz.com/images2/620000/7000/300/627377.jpg'
,'https://www.youtube.com/watch?v=wtJe2MRfBww');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'BMW I8','CAR','BMW',to_date('20170725','YYYYMMDD'),'NO',17500000,'4 SEAT HYBRID CAR',
'http://www.mphclub.com/wp-content/uploads/2015/09/bmw-i8-slider-12.jpg',
'https://www.youtube.com/watch?v=EoojNzIL2cQ');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'SUZUKI GIXXER','SUZUKI','MOTORBIKE',to_date('20170325','YYYYMMDD'),'NO',215000,'150cc AND GOOD PERFORMANCE',
'https://upload.wikimedia.org/wikipedia/commons/d/d6/Suzuki_Gixxer.jpg'
,'https://www.youtube.com/watch?v=s2gHQ8IuKrY');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'ASUS ROG G SERIES','LAPTOP','ASUS',to_date('20170125','YYYYMMDD'),'2 YEARS',295000,'RAM-32GB, GRAPHICS 8GB DDR5',
'https://cdn.istanbulbilisim.com/p/0/asus-rog-g-series-g752vs-gc165t-gaming-notebook_228053.jpg',
'https://www.youtube.com/watch?v=bEWmgGc-Q30');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'RAZOR BLADE PRO','LAPTOP','RAZOR',to_date('20160225','YYYYMMDD'),'2 YEARS',230000,'RAM-32GB, 1TB SSD',
'https://d1urewwzb2qwii.cloudfront.net/sys-master/root/h49/h97/8885527347230/83da4f531786d98fc940d41c9f0c505d-razer-blade-pro-gallery-08.jpg'
,'https://www.youtube.com/watch?v=Ws7S5lXw7zk');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'MACBOOK PRO 2017','LAPTOP','APPLE',to_date('20170725','YYYYMMDD'),'2 YEARS',277000,'RAM-16GB DDR3, 2TB ULTRA FAST SSD',
'https://photos5.appleinsider.com/gallery/22556-27423-13-inch-macbook-pro-non-touchbar-l.jpg'
,'https://www.youtube.com/watch?v=1UlyxA9M1N4');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB','https://www.google.com/search?q=primo+rx+4&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWh6mTyfPXAhXMu48KHQIfDV0Q_AUICygC&biw=1366&bih=662#imgrc=FDX_dMYNP8MTjM:');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB','https://www.google.com/search?q=primo+rx+4&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWh6mTyfPXAhXMu48KHQIfDV0Q_AUICygC&biw=1366&bih=662#imgrc=FDX_dMYNP8MTjM:');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB','https://www.google.com/search?q=primo+rx+4&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWh6mTyfPXAhXMu48KHQIfDV0Q_AUICygC&biw=1366&bih=662#imgrc=FDX_dMYNP8MTjM:');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB','https://www.google.com/search?q=primo+rx+4&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWh6mTyfPXAhXMu48KHQIfDV0Q_AUICygC&biw=1366&bih=662#imgrc=FDX_dMYNP8MTjM:');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB','https://www.google.com/search?q=primo+rx+4&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWh6mTyfPXAhXMu48KHQIfDV0Q_AUICygC&biw=1366&bih=662#imgrc=FDX_dMYNP8MTjM:');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB','https://www.google.com/search?q=primo+rx+4&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWh6mTyfPXAhXMu48KHQIfDV0Q_AUICygC&biw=1366&bih=662#imgrc=FDX_dMYNP8MTjM:');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB','https://www.google.com/search?q=primo+rx+4&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWh6mTyfPXAhXMu48KHQIfDV0Q_AUICygC&biw=1366&bih=662#imgrc=FDX_dMYNP8MTjM:');

Insert into PRODUCT(PRODUCT_ID ,PRODUCT_NAME,PRODUCT_TYPE,BRAND,RELEASE_DATE,WARRENTY,PRICE,PRODUCT_DESCRIPTION,IMAGE,VIDEO) values 
(for_product.NEXTVAL,'Primo RX-4','PHONE','WALTON',to_date('20100725','YYYYMMDD'),'2 YEARS',12000.99,'RAM-2GB,memory-8GB','https://www.google.com/search?q=primo+rx+4&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWh6mTyfPXAhXMu48KHQIfDV0Q_AUICygC&biw=1366&bih=662#imgrc=FDX_dMYNP8MTjM:');



























































select * from product;
SELECT PRODUCT_NAME FROM PRODUCT WHERE PRODUCT_NAME LIKE '%ASU%';

commit;



Insert into ORDER_TABLE(ORDER_ID ,ORDER_CUSTOMER_NO,ORDER_SUPPLIER_NO,ORDER_PRODUCT_NO,TRANSPORTER_NO,AMOUNT,MONEY,SUPPLIER_PIN,CUSTOMER_PIN) values 
(for_history.NEXTVAL,123,33,13,223,2,(SELECT PRICE FROM PRODUCT WHERE PRODUCT_ID=13)*2,supplier_pin.nextval,customer_pin.nextval);
Insert into ORDER_TABLE(ORDER_ID ,ORDER_CUSTOMER_NO,ORDER_SUPPLIER_NO,ORDER_PRODUCT_NO,TRANSPORTER_NO,AMOUNT,MONEY,SUPPLIER_PIN,CUSTOMER_PIN) values 
(for_history.NEXTVAL,124,34,14,223,2,(SELECT PRICE FROM PRODUCT WHERE PRODUCT_ID=14)*2,supplier_pin.nextval,customer_pin.nextval);
Insert into ORDER_TABLE(ORDER_ID ,ORDER_CUSTOMER_NO,ORDER_SUPPLIER_NO,ORDER_PRODUCT_NO,TRANSPORTER_NO,AMOUNT,MONEY,SUPPLIER_PIN,CUSTOMER_PIN) values 
(for_history.NEXTVAL,125,35,15,223,2,(SELECT PRICE FROM PRODUCT WHERE PRODUCT_ID=15)*2,supplier_pin.nextval,customer_pin.nextval);
Insert into ORDER_TABLE(ORDER_ID ,ORDER_CUSTOMER_NO,ORDER_SUPPLIER_NO,ORDER_PRODUCT_NO,TRANSPORTER_NO,AMOUNT,MONEY,SUPPLIER_PIN,CUSTOMER_PIN) values 
(for_history.NEXTVAL,126,35,13,223,2,(SELECT PRICE FROM PRODUCT WHERE PRODUCT_ID=13)*2,supplier_pin.nextval,customer_pin.nextval);
Insert into ORDER_TABLE(ORDER_ID ,ORDER_CUSTOMER_NO,ORDER_SUPPLIER_NO,ORDER_PRODUCT_NO,TRANSPORTER_NO,AMOUNT,MONEY,SUPPLIER_PIN,CUSTOMER_PIN) values 
(for_history.NEXTVAL,127,34,14,223,2,(SELECT PRICE FROM PRODUCT WHERE PRODUCT_ID=14)*2,supplier_pin.nextval,customer_pin.nextval);


Insert into BUY_SELL_HISTORY(BUY_SELL_HISTORY_ID,HISTORY_CUSTOMER_NO,HISTORY_SUPPLIER_NO,HISTORY_PRODUCT_NO,HISTORY_TRANSPORTER_NO,HISTORY_AMOUNT,HISTORY_MONEY,ENTRY_DATE) values
(for_history.NEXTVAL,127,34,14,223,2,1100,sysdate);
select * from order_table;
commit;


SELECT *FROM PRODUCT;

--Trigger
